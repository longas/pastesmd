# Pastesmd
Proyecto final para Desarrollo de Aplicaciones Web cursado en CPIFP Los Enlaces desarrollado por **Gabriel Longás**.

## Instalación y configuración
Para el funcionamiento del proyecto hacen falta las siguientes dos cosas:

* [node.js](http://nodejs.org/), para instalarlo simplemente abre el ejecutable que te descargues desde la página.
* [MongoDB](http://www.mongodb.org/), una vez [descargada](http://www.mongodb.org/downloads) la versión correcta para tu sistema operativo simplemente sigue [las instrucciones para su instalación](http://docs.mongodb.org/manual/installation/).

Una vez descargado el proyecto del repositorio, podrás ver que contiene 2 carpetas principales:

* **app**: Es donde se encuentra la totalidad del código de la aplicación, desde el back-end hasta el front-end.
* **docs**: La documentación creada para el proyecto, en formato Markdown, PDF y HTML.

Para instalar los modulos necesarios para la ejecución de la aplicación vamos a hacer uso de node.js y su herramienta de administraciñon de paquetes y dependencias **npm**. Ejecutando el archivo **package.json** haremos que se instalen las dependencias necesarias.

Con la consola nos colocamos en el directorio **app** (donde se encuentra el archivo **package.json**) y ejecutamos el comando:

    npm install

Una vez ejecutado el comando podremos ver como se están instalando todos los componentes. Cuando se haya terminado de descargar todo, podremos ver como se ha creado una carpeta nueva llamada **node_modules** en la cual se encuentran todas las dependencias necesarias.

Con todos los elementos de la aplicación preparados solo queda ejecutarla. Primero necesitamos encender la base de datos MongoDB que previamente hemos instalado, para hacer esto solo necesitamos ejecutar el siguiente comando:

    mongod

Una vez MongoDB este encendido, con la consola nos vamos a la carpeta **app* y usamos el siguiente comando:

    node server

Ya solo queda abrir el navegador e irnos a la dirección `http://localhost:8080` para poder empezar a usar la aplicación.
