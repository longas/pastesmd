# Pastesmd
### Desarrollo de Aplicaciones Web
Gabriel Longás Calvo - 2ºDAW

[Repositorio en Bitbucket](https://bitbucket.org/longas/pastesmd)

\newpage

# Índice
* 1. Objetivo y detalles del proyecto - ***Pág. 3***
* 2. Documento de análisis y diseño - ***Pág. 4***
    * 2.1 Diseño - ***Pág. 4***
    * 2.2 Análisis - ***Pág. 4***
* 3. Documentación de instalación y configuración - ***Pág. 5***
* 4. Documentación de usuario - ***Pág. 9***
    * 4.1 Cabeceras - ***Pág. 9***
    * 4.2 Formato - ***Pág. 10***
    * 4.3 Listas - ***Pág. 10***
    * 4.4 Enlaces - ***Pág. 11***
    * 4.5 Imágenes - ***Pág. 11***
    * 4.6 Citas - ***Pág. 11***
    * 4.7 Código - ***Pág. 11***
    * 4.8 Lineas horizontales - ***Pág. 12***
* 5. Contrato del proyecto - ***Pág. 13***
    * 5.1 Lista de requisitos - ***Pág. 13***
    * 5.2 Presupuesto detallado - ***Pág. 13***
    * 5.3 Planificación temporal con hitos - ***Pág. 13***
    * 5.4 Diagrama de tareas - ***Pág. 14***
    * 5.5 Pliego de condiciones - ***Pág. 14***
* 6. Diario de bitácora - ***Pág. 15***
* 7. Bibliografía - ***Pág. 16***

\newpage

# 1. Objetivo y detalles del proyecto
The goal of this project is to create a website where users can create and share documents using Markdown, in an easy and intuitive way.

Markdown is a plain text formatting syntax. It's popularly used to format readme files, for writing messages in online discussion forums or in text editors for the quick creation of rich text documents.

The project consist of two parts. The first one is in charge of parsing the Markdown, converting it to HTML and preparing the document, all these actions happen in the client meaning that it's written in Javascript, thanks to that you can implement it in any website regardless of the language used in the server. The other part is the back-end, it's written entirely in Javascript too, using node.js as the scripting framework and MongoDB for the database platform.

To create a document the user only needs to enter a title, type the markdown text and choose between one of the templates available. Once the document is saved, the application will redirect the user to his newly created document where he will be able to copy the URL and share it with anyone.

This service resembles to Pastebin, but instead of sharing code snippets you can create and share rich documents written in Markdown.

\newpage

# 2. Documento de análisis y diseño
## 2.1 Análisis
**Markdown** es un lenguaje de marcado ligero que trata de conseguir la máxima legibilidad y *"publicabilidad"* tanto en sus forma de entrada como de salida, inspirándose en muchas convenciones existentes para marcar mensajes de correo electrónico usando texto plano. Al principio se usaba para crear documentos simples, pero con el tiempo y la mayor adopción que ha tenido, se han ido creando diferentes servicios que hacen uso del lenguaje. Desde escribir libros, crear diapositivas o escribir manuales.

Con **Pastesmd** se pretende crear un servicio donde los usuarios puedan crear y compartir documentos en formato web (HTML) pero escritos previamente en Markdown, de la manera mas sencilla y simple posible.

El proyecto esta separado en 2 partes bien diferenciadas. El *back-end* es el encargado de guardar los documentos creados por los usuarios para luego poder acceder a ellos, la otra parte es la encargada de *parsear* y "traducir" esos textos escritos en Markdown a HTML. Ambas están escritas en Javascript, la razón principal principal es que en la empresa donde he hecho el Erasmus usaban también Javascript tanto en el cliente como en el servidor. La otra razón en el caso del *parser* es que quería que se ejecutase en el cliente ya que eso significa que se puede usar en cualquier página web sin importar el como este programado el *back-end*.

## 2.2 Diseño
Como se ha comentado anteriormente, uno de los principales objetivos de la aplicación es que sea intuitiva y sencilla de utilizar.

Una vez el usuario este en la aplicación, se encontrará delante de un formulario donde se le pedirá que introduzca el título del documento y el texto de este, además de tener que elegir entre uno de los templates que haya disponibles. Para ayudar a personas que nunca hayan usado Markdown anteriormente, se mostrará una guía en la misma página sobre como se tiene que formatear el texto.

Una vez el usuario haya dado al boton de guardar, la aplicación lo redigirá a la dirección en la cual se ha creado su documento para que la pueda copiar y compartir con cualquier persona.

\newpage

# 3. Documentación de instalación, configuración y desarrollo software
Para el funcionamiento del proyecto hacen falta las siguientes dos cosas:

* [node.js](http://nodejs.org/), para instalarlo simplemente abre el ejecutable que te descargues desde la página.
* [MongoDB](http://www.mongodb.org/), una vez [descargada](http://www.mongodb.org/downloads) la versión correcta para tu sistema operativo simplemente sigue [las instrucciones para su instalación](http://docs.mongodb.org/manual/installation/).

Una vez descargado el proyecto del repositorio, podrás ver que contiene 2 carpetas principales:

* **app**: Es donde se encuentra la totalidad del código de la aplicación, desde el back-end hasta el front-end.
* **docs**: La documentación creada para el proyecto, en formato Markdown, PDF y HTML.

En la carpeta que nos vamos a centrar es **app**, una vez dentro de ella podrás ver los siguientes ficheros:

![Carpeta **app**](images/1.png)

Para instalar los modulos necesarios para la ejecución de la aplicación vamos a hacer uso de node.js y su herramienta de administraciñon de paquetes y dependencias **npm**. Ejecutando el archivo **package.json** haremos que se instalen las dependencias necesarias.

\newpage

El código de **package.json** es el siguiente:

    {
        "name": "paste-smd",
        "main": "server.js",
        "dependencies": {
            "express": "~4.0.0",
            "jade": "*",
            "mongoose": "~3.6.13",
            "body-parser": "~1.0.1"
        }
    }

Con la consola nos colocamos en el directorio donde se encuentra el archivo **package.json** y ejecutamos el comando:

    npm install

Una vez ejecutado el comando podremos ver como se están instalando todos los componentes.

![](images/2.png)

\newpage

Una vez se haya terminado de descargar todo, en la carpeta **app** veremos como se ha creado una carpeta nueva llamada **node_modules** en la cual se encuentran todas las dependencias necesarias.

![](images/3.png)

Con todos los elementos de la aplicación preparados solo queda ejecutarla. Primero necesitamos encender la base de datos MongoDB que previamente hemos instalado, para hacer esto solo necesitamos ejecutar el siguiente comando:

    mongod

Una vez MongoDB este encendido, con la consola nos vamos a la carpeta **app** y usamos el siguiente comando:

    node server

Si todo se ha ejecutado sin problemas deberemos ver el siguiente mensaje en consola:

![](images/4.png)

\newpage

Ya solo queda abrir el navegador e irnos a la dirección `http://localhost:8080` para poder empezar a usar la aplicación.

![Pastesmd en ejecución](images/5.png)

\newpage

# 4. Documentación de usuario
Pastesmd usa una implementación de Markdown que sigue al 99% su sintaxis, se han cambiado y añadido algunos detalles para poder añadir mas elementos a los documentos. El uso de la aplicación es muy sencillo, una vez el usuario este en ella se encontrara con un menú a la izquierda que sirve de guía de sintaxis y a la derecha un formulario donde se le pide el título, texto y template.

![](images/5.png)

A continuación se enseña la sintaxis:

## 4.1 Cabeceras
Los encabezamientos se crean colocando un número determinado de almohadillas (#) antes del texto correspondiente al nivel de encabezamiento deseado (HTML ofrece hasta seis niveles), además se ha añadido la opción de especificar una ID que será usada para generar un menú en el cual poder navegar entre las secciones del documentos de una forma mas sencilla. Los encabezamientos posibles se pueden ver en la siguiente tabla:

    # id=seccion1 Encabezado
    ## Encabezado
    ### Encabezado
    #### Encabezado
    ##### Encabezado
    ###### Encabezado

Esto producirá:

    <h1 id="seccion1">Encabezado</h1>
    <h2>Encabezado</h2>
    <h3>Encabezado</h3>
    <h4>Encabezado</h4>
    <h5>Encabezado</h5>
    <h6>Encabezado</h6>

## 4.2 Formato
El formato básico del texto, es decir negritas y cursiva, se pueden realizar de la siguiente manera:

    *Esto es cursiva*
    **Esto es negrita**

Esto producirá:

    <em>Esto es cursiva</em>
    <strong>Esto es negrita</strong>

## 4.3 Listas
Para crear listas simplemente hay que seguir el siguiente formato:

    @
    - Primer elemento
    - Segundo elemento
    - Tercer elemento
    @@

Esto producirá:

    <ul>
        <li>Primer elemento</li>
        <li>Segundo elemento</li>
        <li>Tercer elemento</li>
    </ul>

## 4.4 Enlaces
Crear enlaces es muy facil:

    [Google](http://google.com)

Esto producirá:

    <a href="http://google.com">Google</a>

## 4.5 Imágenes
La manera de incluir imágenes es muy similar a la de los enlaces:

    ![Alt texto](/ruta/img.png)

Esto producirá:

    <img src="/ruta/img.png" alt="Alt texto">

## 4.6 Citas
Para crear bloques de cita, se emplea el carácter mayor que (>) antes del bloque de texto:

    > Esto es una cita.

Esto producirá:

    <blockquote>Esto es una cita.</blockquote>

## 4.7 Código
Para incluir código en nuestros documentos debemos encerrar el texto entre dos acentos graves (`):

    Esto es un párrafo normal, `con un trozo de código`

Esto producirá:

    Esto es un párrafo normal, <code>con un trozo de código</code>

## 4.8 Lineas horizontales
Para crear líneas horizontales se debe crear una línea rodeada de líneas en blanco y compuesta por 5 o más guiones (-):

    -----

Esto producirá:

    <hr>

\newpage

# 5. Contrato del proyecto

## 5.1 Lista de requisitos
* La aplicación debe ser simple e intuitiva de usar.
* Debe de haber una guía de sintaxis para ayudar a los usuarios.
* Se tiene que poder especificar un título y un template a la hora de crear el documento.
* Los documentos generados deben contener un menú mediante el cual los usuarios puedan navegar de una forma más cómoda.
* Los documentos deben de poder ser facilmente compartidos.
* El sistema de generación de documentos tiene que poderse incluir en otras plataformas de forma sencilla y teniendo que modificar las menos cosas posibles.
* Todos los textos de la aplicación deberán estar en inglés.

## 5.2 Presupuesto detallado
* El coste por hora trabajada es de **35 Euros**.
* Se necesitarán **45 horas** para la finalización del proyecto.
* El coste de los dispositivos informáticos para desarrollar y *testear* la aplicación es de **1200 Euros**.

El coste total del proyecto es:

* 35 Euros * 45 horas = 1575 Euros
* 1575 Euros + 1200 Euros = **2775 Euros es el precio total**

## 5.3 Planificación temporal con hitos
* El desarrollo de la aplicación comienza el día **19 de mayo de 2014**.
* **Hito 1**: El **19 de mayo** comienza la investigación de como poder desarrollar un *parser* en Javascript, para el **día 25** se deberá tener claro como se tiene que empezar.
* **Hito 2**: El **26** se empieza a programar el *parser* y el generador de documentos, esta tarea deberá estar terminada para el **4 de junio**.
* **Hito 3**: Desde el **día 5** al **10** se deberá de programar el sistema de back-end para que los usuarios puedan guardar y compartir sus documentos.

\newpage

## 5.4 Diagrama de tareas
![](images/8.png)

## 5.5 Pliego de condiciones
* Se pagará un 40% al comienzo del desarrollo y el 60% restante al termino de este.
* La forma de pago será mediante transferencia bancaria.
* Compromiso a la entrega del proyecto segun la planificación temporal, en caso de no ser así el cliente se guarda el derecho de recibir un compensación económica.
* Las Partes podrán modificar o dar por terminado el presente Acuerdo por mutuo acuerdo y por escrito dentro del periodo de vigencia del mismo.
* Cada una de las Partes se compromete a no difundir, bajo ningún concepto, las informaciones científicas o técnicas pertenecientes a la otra parte, a las que haya podido tener acceso con ocasión del desarrollo del Proyecto.

\newpage

# 6. Diario de bitácora
* El día **19 de mayo** comienzo el proyecto. El primer paso es investigar diferentes formas de poder crear un *parser* y luego como poder hacerlo en Javascript para que se ejecute en el cliente. A lo largo de esa semana encuentro que la mayoría de los *parsers* usan expresiones regulares para poder analizar el texto de manera rápida.
* La semana **del 19 al 25** la paso estudiando como funcionan las expresiones regulares y patrones de diseño comunes en Javascript para crear y organizar proyectos.
* Una vez dispongo de una base buena, el **día 26** empiezo a programar el *parser*. Durante esa semana lo vuelvo a empezar a hacer de 0 un par de veces ya que me costaba entender algunas cosas de las expresiones regulares que anteriormente pensaba haber comprendido.
* Hasta el día 6 no consigo tener terminado tanto el *parser* como el generador de documentos, 2 días mas tarde de lo previsto pero nada que no se pueda salvar.
* Del **7** al **10** consigo tener terminado el back-end y por lo tanto la fecha limite para terminar el trabajo se cumple.

\newpage

# 7. Bibliografía
* [Markdown](http://daringfireball.net/projects/markdown)
* [Regular Expressions](https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions) | MDN
* [JavaScript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference) | MDN
* [Eloquent JavaScript](http://eloquentjavascript.net/)
* [JavaScript: The Good Parts](http://www.amazon.com/JavaScript-Good-Parts-Douglas-Crockford/dp/0596517742)
* [Prototypes and Inheritance in JavaScript](http://msdn.microsoft.com/en-us/magazine/ff852808.aspx)
* [JavaScript Module Pattern: In-Depth](http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html)
* [Asynchronous JS: Callbacks, Listeners, Control Flow Libs and Promises](http://sporto.github.io/blog/2012/12/09/callbacks-listeners-promises/)
