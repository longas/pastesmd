// Detect scroll behaviours
setTimeout(function() {
    var topOffset = window.pageYOffset || document.documentElement.scrollTop;
    var menu = document.getElementsByTagName('nav')[0];
    var menuLinks = menu.getElementsByTagName('a');
    var menuTopOffset = menu.offsetTop;
    var scrolltop = document.getElementById('scrolltop')

    // Change target position for menu links
    for (var i = 0; i < menuLinks.length; i++) {
        menuLinks[i].onclick = function(e) {
            e.preventDefault();

            var linkHref = this.href;
            var linkHrefId = linkHref.split(/#(.*)/)[1];
            var target = document.getElementById(linkHrefId).offsetTop;
            var targetPosition = target - (menu.clientHeight + 10);

            window.scrollTo(0, targetPosition);
        };
    }

    // Move to the top of the page if scrolltop is clicked
    scrolltop.onclick = function(e) {
        window.scrollTo(0, 0);
    };

    // Stick top menu and show scrolltop
    if (topOffset > menuTopOffset) {
        menu.style.position = 'fixed';
        menu.style.top = 0;

        scrolltop.style.display = 'block';
    }

    // Detect scroll
    window.onscroll = function(event) {
        topOffset = window.pageYOffset || document.documentElement.scrollTop;

        // Stick top menu and show scrolltop
        if (topOffset > menuTopOffset) {
            menu.style.position = 'fixed';
            menu.style.top = 0;

            scrolltop.style.display = 'block';
        } else {
            menu.style.position = 'absolute';
            menu.style.top = 'auto';

            scrolltop.style.display = 'none';
        }
    };
}, 150);
