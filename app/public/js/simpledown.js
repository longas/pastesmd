//
// simpledown.js - A Markdown parser made with JavaScript.
// Gabriel Longás (c) 2014.
//

function Simpledown() {
    this.rules = {
        blockquote: {
            pattern: />(.*)/g,
            replacement: this.replaceBlockquote
        },
        header: {
            pattern: /^(#+)( id=\w*)?(.*)/gm,
            replacement: this.replaceHeader
        },
        image: {
            pattern: /!\[(.*?)\]\((.*?)\)/g,
            replacement: this.replaceImage
        },
        link: {
            pattern: /\[(.*?)\]\((.*?)( id=\w*)?\)/g,
            replacement: this.replaceLink
        },
        bold: {
            pattern: /(\*\*)(.*?)\1/g,
            replacement: '<strong>$2</strong>'
        },
        emphasis: {
            pattern: /(\*)(.*?)\1/g,
            replacement: '<em>$2</em>'
        },
        code: {
            pattern: /`(.*?)`/g,
            replacement: '<code>$1</code>'
        },
        hrule: {
            pattern: /-{5,}/g,
            replacement: '<hr>'
        },
        ul: {
            pattern: /^(@+)/gm,
            replacement: this.replaceUl
        },
        li: {
           pattern: /^-(.*)/gm,
           replacement: this.replaceLi
        },
        paragraph: {
            pattern: /^([^\n]+)/gm,
            replacement: this.replaceParagraph
        }
    }
}


///////////////////////////
// Replacement functions //
///////////////////////////

Simpledown.prototype.replaceHeader = function() {
    var level = arguments[1].length > 6 ? 6 : arguments[1].length;
    var id = arguments[2];
    var text = arguments[3].trim();

    var beginTag = '<h' + level + '>';

    if (id) {
        id = id.trim().slice(3);
        beginTag = '<h' + level + ' id="' + id + '">'
    }

    return beginTag + text + '</h' + level + '>';
};

Simpledown.prototype.replaceImage = function() {
    var src = arguments[2].trim();
    var alt = arguments[1].trim();

    return '<img src="' + src + '" alt="' + alt + '">';
};

Simpledown.prototype.replaceLink = function() {
    var text = arguments[1].trim();
    var url = arguments[2].trim();
    var id = arguments[3];

    var beginTag = '<a href="' + url + '">';

    if (id) {
        id = id.trim().slice(3);
        beginTag = '<a id="' + id + '" href="' + url + '">';
    }

    return beginTag + text + '</a>';
};

Simpledown.prototype.replaceBlockquote = function() {
    var text = arguments[1].trim();
    return '<blockquote>' + text + '</blockquote>';
};

Simpledown.prototype.replaceUl = function() {
    var ammount = arguments[1].length > 2 ? 2 : arguments[1].length;

    if (ammount === 1) {
        return '<ul>';
    } else {
        return '</ul>';
    }
};

Simpledown.prototype.replaceLi = function() {
    var text = arguments[1].trim();
    return '<li>' + text + '</li>';
};

Simpledown.prototype.replaceParagraph = function() {
    var text = arguments[1].trim();

    if (/^<h|<block/gm.test(text)) {
        return text;
    }

    return '<p>' + text + '</p>';
};


//////////////////////////////
// Simpledown functionality //
//////////////////////////////

Simpledown.prototype.addRule = function(name, regex, replacement) {
    this.rules[name] = {
        pattern: regex,
        replacement: replacement
    };
};

Simpledown.prototype.render = function(text) {
    var output = text.trim();

    for (var rule in this.rules) {
        var rule = this.rules[rule];
        var regex = rule.pattern;
        var replacement = rule.replacement;

        output = output.replace(regex, replacement);
    }

    return output;
};
