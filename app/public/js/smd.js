(function() {
    // Themes
    var themes = {
        simple: {
            styles: [
                'themes/simple/css/normalize.css',
                'themes/simple/css/main.css',
                '//fonts.googleapis.com/css?family=Droid+Serif:400,700',
                '//fonts.googleapis.com/css?family=Vollkorn:400,700'
            ],
            scripts: []
        },
        laravel: {
            styles: [
                'themes/laravel/css/normalize.css',
                'themes/laravel/css/main.css',
                '//fonts.googleapis.com/css?family=Lato:100,300,400,700,900'
            ],
            scripts: [
                'themes/laravel/js/theme.js'
            ]
        }
    };

    // Elements we need
    var documentHead = document.getElementsByTagName('head')[0];
    var documentBody = document.getElementsByTagName('body')[0];
    var smdContent = document.getElementById('smd');

    // Get the selected theme
    var selectedTheme = smdContent.getAttribute('data-theme');
    if (!selectedTheme) {
        console.error("SMD: You need to select a theme");
        return;
    }

    var theme = themes[selectedTheme];
    if (!theme) {
        console.error("SMD: The selected theme doesn't exist!");
        return;
    }

    // Load theme styles
    var themeStyles = theme.styles;
    var themeStylesLength = themeStyles.length;
    if (themeStylesLength) {
        for (var i = 0; i < themeStylesLength; i++) {
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.href = themeStyles[i];
            documentHead.appendChild(style);
        }
    }

    // Load theme scripts
    var themeScripts = theme.scripts;
    var themeScriptsLength = themeScripts.length;
    if (themeScriptsLength) {
        for (var i = 0; i < themeScriptsLength; i++) {
            var script = document.createElement('script');
            script.src = themeScripts[i];
            documentHead.appendChild(script);
        }
    }

    // Parse the markdown
    var simpledown = new Simpledown();
    var markdownText = smdContent.textContent;
    var parsedText = simpledown.render(markdownText);

    // Prepare the DOM
    smdContent.parentNode.removeChild(smdContent);
    var smdWrap = document.createElement('div');
    smdWrap.setAttribute('id', 'wrap');
    documentBody.insertBefore(smdWrap, documentBody.firstChild);

    // Create header
    var header = document.createElement('header');
    var headerWrap = document.createElement('h1');
    headerWrap.innerHTML = document.title;
    header.appendChild(headerWrap);

    // Create content area
    var content = document.createElement('div');
    content.setAttribute('id', 'content');
    content.innerHTML = parsedText;

    // Create menu
    var menu = document.createElement('nav');
    var h1s = content.getElementsByTagName('h1');
    var h1sLength = h1s.length;
    for (var i = 0; i < h1sLength; i++) {
        var id = h1s[i].getAttribute('id');

        if (id) {
            var menuLink = document.createElement('a');
            menuLink.href = '#' + h1s[i].getAttribute('id');
            menuLink.innerHTML = h1s[i].innerHTML;
            menu.appendChild(menuLink);
        }
    }

    // Create footer
    var footer = document.createElement('footer');
    footer.innerHTML = 'Document generated with <a href="#">smd.js<a>';

    // Create scrollToTop area
    var scrolltop = document.createElement('div');
    scrolltop.setAttribute('id', 'scrolltop');

    // Add the elements to the page
    smdWrap.appendChild(header);
    smdWrap.appendChild(menu);
    smdWrap.appendChild(content);
    smdWrap.appendChild(footer);
    smdWrap.appendChild(scrolltop);
}());
