$(document).ready(function() {
    var $accordions = $('#accordion .data');
    var $accordionItems = $('#accordion .item');

    $accordionItems.click(function() {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).next().slideUp();
        } else {
            $accordions.slideUp();
            $accordionItems.removeClass('open');
            $(this).addClass('open');
            $(this).next().slideDown();
            return false;
        }
    });
});
