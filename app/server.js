// BASE SETUP
// ===============================================
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

var port = process.env.PORT || 8080;

// DATABASE
// ===============================================
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MdocSchema = new Schema({
	title: String,
	md: String,
	template: String
});

var Mdoc = mongoose.model('Mdoc', MdocSchema);

mongoose.connect('mongodb://localhost/pastesmd');

// ROUTES
// ===============================================
app.get('/', function(req, res) {
	res.render('index');
});

app.post('/', function(req, res) {
	var mdoc = new Mdoc();
	mdoc.title = req.body.title;
	mdoc.md = req.body.md;
	mdoc.template = req.body.template;

	mdoc.save(function(err, mdoc) {
		if (err) res.send(err);

		res.redirect(mdoc._id);
	});
});

app.get('/:md_id', function(req, res) {
	Mdoc.findById(req.params.md_id, function(err, mdoc) {
		if (err) res.redirect('/');

		res.render('document', mdoc);
	});
});

// START THE SERVER
// ===============================================
app.listen(port);
console.log('Magic happens on port ' + port);
